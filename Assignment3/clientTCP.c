#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "myfunction.h"

#define LOCALHOST "127.0.0.1"

//messages
#define OK_READY "200 OK - Ready"
#define INVALID_MESSAGE_ERROR "404 ERROR – Invalid Hello message"
#define INVALID_MEASUREMENT_MESSAGE "404 ERROR – Invalid Measurement message"
#define BYE_MESSAGE "b\n"
#define CORRECT_CLOSURE "200 OK – Closing"

//<measure_type>
#define RTT "rtt"
#define THROUGHPUT "thput"
char measure_type[30];

//<protocol_phase>
#define HELLO_PHASE "h"
#define MEASUREMENT_PHASE "m"
#define BYE_PHASE "b"

//<n_probes>
#define MIN_MESSAGES 20
long n_probes;

//<msg_size>
int msg_size;

//<server_delay>
int server_delay;

#define MAX_BUF_SIZE 40000
#define MAX_BUF_PAYLOAD 35000

void create_payload(char *payload);
void hello_phase_error(int fd);

int main(int argc, char *argv[])
{

	struct sockaddr_in server_addr;  // Struct containing server address information
	struct sockaddr_in client_addr;  // Struct containing client address information
	int sfd;						 // Server socket filed descriptor
	int cr;							 // Connect result
	ssize_t byteRecv;				 // Number of bytes received
	ssize_t byteSent;				 // Number of bytes to be sent
	socklen_t serv_size;			 // Size of the server data structure
	char receivedData[MAX_BUF_SIZE]; // Data to be received
	char sendData[MAX_BUF_SIZE];	 // Data to be sent
	long probe_seq_num;				 // Number of probe to be sent
	char payload[MAX_BUF_PAYLOAD];   // Payload to send
	double t1, t2, aux_t1, aux_t2;   // Time in seconds for RTT calculation
	struct timeval start, end;		 // Data structure for time for RTT calculation
	double rtt;						 // Round trip time
	double throughput;				 // Throughput
	long size_calc;					 // Size of the message sent including the header
	char *phase;					 // Variable in which the current phase is stored
	char input[101];				 // Buffer for keyboard input
	char *aux;						 // For query parsing

	/***************************************************************************
	 *                                                                         *
	 *                           Connect on server                             *
	 *                                                                         *
	 **************************************************************************/
	sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sfd < 0)
	{
		perror("socket"); // Print error message
		exit(EXIT_FAILURE);
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(atoi(argv[2]));
	server_addr.sin_addr.s_addr = inet_addr(argv[1]);
	serv_size = sizeof(server_addr);

	cr = connect(sfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
	if (cr < 0)
	{
		perror("Connect Error \n"); // Print error message
		exit(EXIT_FAILURE);
	}

	/***************************************************************************
	 *                                                                         *
	 *                           End connect on server                         *
	 *                                                                         *
	 **************************************************************************/

	/***************************************************************************
	 *                                                                         *
	 *                           Hello phase                                   *
	 *                                                                         *
	 **************************************************************************/

	//<protocol_phase> <sp> <measure_type> <sp> <n_probes> <sp> <msg_size> <sp><server_delay>\n
	printf("Type the connection analysis query\n");
	printf("<protocol_phase> <sp> <measure_type> <sp> <n_probes> <sp> <msg_size> <sp> <server_delay>\n");
	read(0, input, 100);
	tcflush(0, TCIFLUSH);
	strcpy(sendData, input);

	/****************
	* parsing query *
	****************/

	//parsing phase
	phase = strtok(input, " ");
	if (phase == NULL)
	{
		perror("\nincorrect phase"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}
	if (strcmp(phase, HELLO_PHASE) != 0)
	{
		perror("\nincorrect phase"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}

	//parsing measure type
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("\nincorrect measure_type"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}
	strcpy(measure_type, aux);
	if (strcmp(measure_type, RTT) != 0 && strcmp(measure_type, THROUGHPUT) != 0)
	{
		perror("\nincorrect measure_type"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}

	//parsing n_probes
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("\nincorrect n_probes"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}
	n_probes = atoi(aux);
	if (n_probes < 20)
	{
		perror("\nincorrect n_probes"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}

	//parsing  msg_size
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("\nincorrect msg_size"); // Print error message
		hello_phase_error(sfd);
		goto send_server;
	}
	msg_size = atoi(aux);
	if (strcmp(measure_type, THROUGHPUT) == 0)
	{
		if (msg_size != 1000 && msg_size != 2000 && msg_size != 4000 && msg_size != 16000 && msg_size != 32000)
		{
			perror("\nincorrect msg_size"); // Print error message
			hello_phase_error(sfd);
			goto send_server;
		}
	}
	else
	{
		if (msg_size != 1 && msg_size != 100 && msg_size != 200 && msg_size != 400 && msg_size != 800 && msg_size != 1000)
		{
			perror("\nincorrect msg_size"); // Print error message
			hello_phase_error(sfd);
			goto send_server;
		}
	}

	//parsing server_delay
	aux = strtok(NULL, " ");
	if (aux == NULL) // if it has not been entered it is set to 0
	{
		perror("\nincorrect server_delay"); // Print error message
		hello_phase_error(sfd);
		server_delay = 0;
	}
	server_delay = atoi(aux);
	if (server_delay < 0) // if a negative value has been entered, set to 0
	{
		perror("\nincorrect server_delay"); // Print error message
		server_delay = 0;
	}
	/********************
	* end parsing query *
	*********************/
send_server:

	printf("\nMessage sent to server: \n%s\n", sendData);
	byteSent = send(sfd, sendData, countStrLen(sendData), 0); // I send the query to the server

	byteRecv = recv(sfd, receivedData, sizeof(receivedData), 0);
	printf("Message received from the server:\n%s\n\n", receivedData);

	if (strcmp(receivedData, OK_READY) != 0) // I verify that the server
	{
		perror("Connect Error into hello phase");
		exit(1);
	}

	/***************************************************************************
	 *                                                                         *
	 *                           end Hello phase                               *
	 *                                                                         *
	 ***************************************************************************/

	/***************************************************************************
	 *                                                                         *
	 *                         Measurement phase                               *
	 *                                                                         *
	 ***************************************************************************/

	// I prepare the measurement phase
	probe_seq_num = 1;
	create_payload(payload);
	probe_seq_num = 1;
	t1 = 0.0;
	t2 = 0.0;

	while (probe_seq_num <= n_probes) // Once n_probes messages have been sent, I must exit the cycle and complete the measurement phase
	{
		// <protocol_phase> <sp> <probe_seq_num> <sp> <payload>\n
		// I create the message of probes to send
		sprintf(sendData, "%s %04ld %s\n", MEASUREMENT_PHASE, probe_seq_num, payload);

		printf("Number of prob message sent to the server: %ld\n", probe_seq_num);
		if (gettimeofday(&start, NULL)) // I take the time in start
		{
			perror("time failed\n"); // Print error message
			exit(1);
		}
		byteSent = send(sfd, sendData, countStrLen(sendData), 0);
		if (byteSent < 0)
		{
			perror("send"); // Print error message
			exit(EXIT_FAILURE);
		}

		byteRecv = recv(sfd, receivedData, sizeof(receivedData), 0);
		if (gettimeofday(&end, NULL)) // I take the time in end
		{
			perror("time failed\n"); // Print error message
			exit(1);
		}
		if (byteSent != byteRecv) // I verify if the sent bytes are the same as those received
		{
			printf("Message received from the server:\n%s\n", receivedData);
			perror("recv"); // Print error message
			exit(EXIT_FAILURE);
		}

		aux_t1 = (start.tv_sec * 1000.0) + (start.tv_usec / 1000.0);
		aux_t2 = (end.tv_sec * 1000.0) + (end.tv_usec / 1000.0);
		printf("prob %ld received\nRTT =\t%g \n\n", probe_seq_num, aux_t2 - aux_t1);

		// I collect the data on the time that I will need to calculate the RTT
		t1 += aux_t1;
		t2 += aux_t2;
		probe_seq_num++;
	} // End of while

	//calculate rtt
	rtt = (t2 - t1) / n_probes;

	//I print the RTT only if measure_type == RTT
	if (strcmp(measure_type, RTT) == 0)
	{
		printf("Average RTT = %g ms\n", rtt);
	}
	else // Otherwise I calculate and print the Throughput
	{
		/*
			<protocol_phase> = 1
			<sp> = 1
			<probe_seq_num> = 4
			<sp> = 1
			<payload> = msg_size
			\n = 1
			'\0' = 1
		*/
		size_calc = (1 + 1 + 4 + 1 + msg_size + 1 + 1); // size in Bit
		throughput = size_calc / rtt;					// size_calc in Bit / rtt in ms == size_calc in KBit / rtt in second == throughput kbps
		printf("Throughput = %g\n", throughput);
	}
	/***************************************************************************
	 *                                                                         *
	 *                         End measurement phase                           *
	 *                                                                         *
	 ***************************************************************************/

	/***************************************************************************
	 *                                                                         *
	 *                               Bye phase                                 *
	 *                                                                         *
	 ***************************************************************************/
	sprintf(sendData, "%s\n", BYE_PHASE); //I build the message si bay
	printf("Message sent to server: \n%s\n", sendData);
	byteSent = send(sfd, sendData, countStrLen(sendData), 0);
	if (byteSent < 0)
	{
		perror("send"); // Print error message
		exit(EXIT_FAILURE);
	}

	memset(receivedData, 0, sizeof(receivedData)); // Buffer reset receivedData

	byteRecv = recv(sfd, receivedData, sizeof(receivedData), 0);
	if (byteSent < 0)
	{
		perror("recv"); // Print error message
		exit(EXIT_FAILURE);
	}
	printf("Message received from the server:\n%s\n", receivedData);
	if (strcmp(receivedData, CORRECT_CLOSURE) != 0) // Check that the closure was successful
	{
		perror("The server doesn't know how to behave\n"); // Print error message
		exit(EXIT_FAILURE);
	}

	/***************************************************************************
	 *                                                                         *
	 *                              End Bye phase                              *
	 *                                                                         *
	 ***************************************************************************/

	close(sfd);
	return 0;
}

void create_payload(char *payload) // I create a large msg_size message including a string terminator
{
	for (int i = 0; i < msg_size - 1; i++)
	{
		payload[i] = 'w';
	}
	payload[msg_size - 1] = '\0';
}

void hello_phase_error(int fd) // Function that manages the errors of the hello phase
{
	char flag;
	printf("Could the query be incorrect, send it anyway? (y or n)\n");
	read(0, &flag, 1);	// Read of input of keyboard
	tcflush(0, TCIFLUSH); // Reset of stdin
	if (flag != 'y')
	{
		close(fd);
		exit(1);
	}
}