#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "myfunction.h"
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#define LOCALHOST "127.0.0.1"

//messages
#define OK_READY "200 OK - Ready"
#define ERROR_HELLO_PHASE "404 ERROR – Invalid Hello message"
#define ERROR_MEASUREMENT_PHASE "404 ERROR – Invalid Measurement message"
#define BYE_MESSAGE "b\n"
#define CORRECT_CLOSURE "200 OK – Closing"

//<measure_type>
#define RTT "rtt"
#define THROUGHPUT "thput"
char measure_type[10];

//<protocol_phase>
#define HELLO_PHASE "h"
#define MEASUREMENT_PHASE "m"
#define BYE_PHASE "b"

//<n_probes>
long n_probes;

//<msg_size>
int msg_size;

//<server_delay>
int server_delay;

#define MIN_MESSAGES 20
#define MAX_BUF_SIZE 40000
#define BACK_LOG 2 // Maximum queued requests

void phase_error(int fd_socket, char string[]);

int main(int argc, char *argv[])
{
	struct sockaddr_in server_addr;  // Struct containing server address information
	struct sockaddr_in client_addr;  // Struct containing client address information
	int sfd;						 // Server socket filed descriptor
	int newsfd;						 // Client communication socket - Accept result
	int br;							 // Bind result
	int lr;							 // Listen result
	int port;						 // Server port
	ssize_t byteRecv;				 // Number of bytes received
	ssize_t byteSent;				 // Number of bytes to be sent
	socklen_t cli_size;				 // Size of the client data structure
	char receivedData[MAX_BUF_SIZE]; // Data to be received
	char sendData[MAX_BUF_SIZE];	 // Data to be sent
	long last_n_probes;				 // Counter of how many probes messages have been sent/received
	char *phase;					 // Variable in which the current phase is stored
	char *aux;						 // Support buffer

	/**************************************************************************
	*                                                                         *
	*                           connect on server                             *
	*                                                                         *
	**************************************************************************/
	sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sfd < 0)
	{
		perror("socket"); // Print error message
		exit(EXIT_FAILURE);
	}

	//port is a number a 16bit when can arrived end a 65535 and can't be negative
	port = atoi(argv[1]);
	if (port < 0 || port > 65535)
	{
		perror("port Error"); // Print error message
		exit(1);
	}
	// Initialize server address information
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);		  // Convert to network byte order
	server_addr.sin_addr.s_addr = INADDR_ANY; // Bind to any address

	br = bind(sfd, (struct sockaddr *)&server_addr, sizeof(server_addr));

	if (br < 0)
	{
		perror("bind"); // Print error message
		exit(EXIT_FAILURE);
	}

	cli_size = sizeof(client_addr);

	// Listen for incoming requests
	lr = listen(sfd, BACK_LOG);
	if (lr < 0)
	{
		perror("listen"); // Print error message
		exit(EXIT_FAILURE);
	}

	// Wait for incoming requests
	newsfd = accept(sfd, (struct sockaddr *)&client_addr, &cli_size);
	if (newsfd < 0)
	{
		perror("accept"); // Print error message
		exit(EXIT_FAILURE);
	}
	close(sfd); // closed welcome socket

	// I print the information of the client that has connected
	printf("Connect to client\nAdress :\t%s\nPort :\t\t%d\n", inet_ntoa(client_addr.sin_addr), client_addr.sin_port);

	/**************************************************************************
	*                                                                         *
	*                           End connect on server                         *
	*                                                                         *
	**************************************************************************/

	/**************************************************************************
	*                                                                         *
	*                           Hello phase                                   *
	*                                                                         *
	**************************************************************************/
	memset(receivedData, 0, sizeof(receivedData)); // Reset receivedData
	//<protocol_phase> <sp> <measure_type> <sp> <n_probes> <sp> <msg_size> <sp><server_delay>\n
	byteRecv = recv(newsfd, receivedData, sizeof(receivedData), 0);
	if (byteRecv < 0)
	{
		perror("Recv error"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
	}

	// I print the information of the client that has connected
	printf("\nMessage received from the client:\n%s\n", receivedData);

	/****************
	* parsing query *
	****************/

	//parsing phase
	phase = strtok(receivedData, " ");
	if (phase == NULL)
	{
		perror("incorrect phase"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}
	if (strcmp(phase, HELLO_PHASE) != 0)
	{
		perror("incorrect phase"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}

	//parsing measure type
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("incorrect measure_type"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}
	strcpy(measure_type, aux);
	if (strcmp(measure_type, RTT) != 0 && strcmp(measure_type, THROUGHPUT) != 0)
	{
		perror("incorrect measure_type"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}

	//parsing n_probes
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("incorrect n_probes"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}
	n_probes = atoi(aux);
	if (n_probes < 20)
	{
		perror("incorrect n_probes"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}

	//parsing  msg_size
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("incorrect msg_size"); // Print error message
		phase_error(newsfd, ERROR_HELLO_PHASE);
		goto send_server;
	}
	msg_size = atoi(aux);
	if (strcmp(measure_type, THROUGHPUT) == 0)
	{
		if (msg_size != 1000 && msg_size != 2000 && msg_size != 4000 && msg_size != 16000 && msg_size != 32000)
		{
			perror("incorrect msg_size"); // Print error message
			phase_error(newsfd, ERROR_HELLO_PHASE);
			goto send_server;
		}
	}
	else
	{
		if (msg_size != 1 && msg_size != 100 && msg_size != 200 && msg_size != 400 && msg_size != 800 && msg_size != 1000)
		{
			perror("incorrect msg_size"); // Print error message
			phase_error(newsfd, ERROR_HELLO_PHASE);
			goto send_server;
		}
	}

	//parsing server_delay
	aux = strtok(NULL, " ");
	if (aux == NULL)
	{
		perror("incorrect server_delay"); // Print error message
		server_delay = 0;
	}
	server_delay = atoi(aux);
	if (server_delay < 0)
	{
		perror("incorrect server_delay"); // Print error message
		server_delay = 0;
	}
	/********************
	* end parsing query *
	********************/
send_server:

	// I send the OK_READY message to the client
	printf("Message sent to client: \n%s\n\n", OK_READY);
	send(newsfd, OK_READY, countStrLen(OK_READY), 0);

	/**************************************************************************
	 *                                                                         *
	 *                           End Hello phase                               *
	 *                                                                         *
	 **************************************************************************/

	/**************************************************************************
	 *                                                                         *
	 *                         Measurement phase                               *
	 *                                                                         *
	 **************************************************************************/

	// I prepare the measurement phase
	last_n_probes = 1;

	/*TEST*/
	// struct timeval start, end;
	// double t1, t2, tot;

	while (last_n_probes <= n_probes)
	{
		memset(receivedData, 0, sizeof(receivedData)); // Reset receivedData
		//<protocol_phase> <sp> <probe_seq_num> <sp> <payload>\n
		byteRecv = recv(newsfd, receivedData, sizeof(receivedData), 0);
		if (byteRecv < 0)
		{
			perror("recv"); // Print error message
			exit(EXIT_FAILURE);
		}

		/*TEST*/
		//gettimeofday(&start, NULL); // I take the time in start

		printf("Message received from the client:\n%s\n", receivedData);
		strcpy(sendData, receivedData);

		/*****************
	 	 * parsing query *
	 	 ****************/
		// parsing MEASUREMENT_PHASE
		if (strcmp(strtok(receivedData, " "), MEASUREMENT_PHASE) != 0)
		{
			perror("MEASUREMENT_PHASE error"); // Print error message
			phase_error(newsfd, ERROR_MEASUREMENT_PHASE);
		}

		// parsing last_n_probes
		if (atoi(strtok(NULL, " ")) != last_n_probes || last_n_probes > n_probes)
		{
			perror("last_n_probes error"); // Print error message
			phase_error(newsfd, ERROR_MEASUREMENT_PHASE);
		}

		//parsing msg_size
		if (countStringLength(strtok(NULL, " ")) != msg_size)
		{
			perror("msg_size error"); // Print error message
			phase_error(newsfd, ERROR_MEASUREMENT_PHASE);
		}
		/********************
		* end parsing query *
	 	********************/
		usleep(server_delay * 1000);			   // I stop for server_delay milliseconds
		memset(receivedData, 0, sizeof(sendData)); // reset receivedData
		printf("Message sent to client: \n%s\n", sendData);

		/*TEST*/
		// gettimeofday(&end, NULL); // I take the time in start
		// t1 = start.tv_sec + (start.tv_usec / 1000000.0);
		// t2 = end.tv_sec + (end.tv_usec / 1000000.0);
		// tot = t2 - t1;

		byteSent = send(newsfd, sendData, countStrLen(sendData), 0); // I send the same package I received to the client
		if (byteSent != byteRecv)
		{
			perror("send"); // Print error message
			exit(EXIT_FAILURE);
		}
		last_n_probes++;
	} // End of while

	/*TEST*/
	// printf("\n\n the time of the last calculation is = %g\n\n", tot);

	/***************************************************************************
	 *                                                                         *
	 *                        End Measurement phase                            *
	 *                                                                         *
	 ***************************************************************************/

	/***************************************************************************
	 *                                                                         *
	 *                               Bye phase                                 *
	 *                                                                         *
	 ***************************************************************************/
	memset(receivedData, 0, sizeof(receivedData)); // reset receivedData
	byteRecv = recv(newsfd, receivedData, sizeof(receivedData), 0);
	if (byteRecv < 0)
	{
		perror("recv"); // Print error message
		exit(EXIT_FAILURE);
	}
	printf("Message received from the client:\n%s\n", receivedData);
	if (strcmp(receivedData, BYE_PHASE "\n") != 0) // parsing message BYE PHASE
	{
		perror("Bye phase not respected"); // Print error message
		phase_error(newsfd, ERROR_MEASUREMENT_PHASE);
	}

	memset(sendData, 0, sizeof(sendData));	// reset sendData
	sprintf(sendData, "%s", CORRECT_CLOSURE); // I create the bye message
	printf("Message sent to client: \n%s\n", sendData);
	byteSent = send(newsfd, sendData, countStrLen(sendData), 0);
	if (byteSent < 0)
	{
		perror("send"); // Print error message
		exit(EXIT_FAILURE);
	}

	/***************************************************************************
	 *                                                                         *
	 *                            End bye phase                                *
	 *                                                                         *
	 ***************************************************************************/

	close(newsfd); // close connection socket
	return 0;
}

// Function that manages errors in the different phases
void phase_error(int fd_socket, char string[])
{
	printf("\nMessage sent to client: \n%s\n", string);
	send(fd_socket, string, countStrLen(string), 0);
	close(fd_socket); // close connection socket
	exit(EXIT_FAILURE);
}