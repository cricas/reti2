#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>

//Constants
#define TCP "tcp"
#define UDP "udp"
#define TIMEOUT 30
#define SERVICES_SIZE 10
#define SERVER_PORT 9000 // Initial server port
#define BACK_LOG 2		 // Maximum number of queued requests

//Service structure
typedef struct
{
	char transport_protocol[20];
	char service_mode[20];
	char port[6];
	char path[200];
	char name[50];
	int fd;
	int pid;
} Service;

///Global variables
int size;
Service services[SERVICES_SIZE];
fd_set allfdset;
fd_set fdset;
int maxfd = 0;

//Function prototypes
int extract_services(Service *services, int size_t);
void getName(char *path, char *name);
int getMaxFd();
//Function prototype devoted to handle the death of the son process
void handle_signal(int sig);

int main(int argc, char **argv, char **env)
{
	// Other variables declaration
	size = extract_services(services, SERVICES_SIZE);
	int i;
	int br;
	int sfd;
	int lr;
	int temp;
	int count = 0;
	struct timeval timeout;
	int clientfd;
	struct sockaddr_in client_addr;
	struct sockaddr_in server_addr;
	socklen_t cli_size;
	pid_t pid;

	// Server behavior implementation
	timeout.tv_sec = 30;
	timeout.tv_usec = 0;
	FD_ZERO(&fdset);

	printf("\nAVAIABLE SERVICES \n");

	//Sockets configuration
	for (i = 0; i < size; i++)
	{
		Service actual_service = services[i];
		if (strcmp(actual_service.transport_protocol, UDP) == 0)
		{
			//Create an UDP socket
			sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			if (sfd < 0)
			{
				perror("socket"); // Print error message
				exit(EXIT_FAILURE);
			}
		}
		else if (strcmp(actual_service.transport_protocol, TCP) == 0)
		{
			//Create a TCP welcome socket
			sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (sfd < 0)
			{
				perror("socket"); // Print error message
				exit(EXIT_FAILURE);
			}
		}

		// Initialize server address information
		server_addr.sin_family = AF_INET;
		server_addr.sin_port = htons(atoi(actual_service.port)); // Convert to network byte order
		server_addr.sin_addr.s_addr = INADDR_ANY;				 // Bind to any address
		br = bind(sfd, (struct sockaddr *)&server_addr, sizeof(server_addr));

		if (strcmp(actual_service.transport_protocol, TCP) == 0)
		{
			//Make the TCP welcome socket ready to accept requests (listening)
			lr = listen(sfd, BACK_LOG);
			if (lr < 0)
			{
				perror("listen"); // Print error message
				exit(EXIT_FAILURE);
			}
		}

		services[i].fd = sfd;
		FD_SET(sfd, &fdset);

		//Print the information about this service
		printf("%d: %s %s port:%s mode:%s fd:%d\n", i, services[i].path, services[i].transport_protocol, actual_service.port, services[i].service_mode, services[i].fd);
	}

	//Initialize the copy of the complete set of file descriptors for the activable services
	allfdset = fdset;

	//Call the function for SIGCHLD handling
	if (signal(SIGCHLD, handle_signal) == SIG_ERR)
	{
		perror("error in signal"); // Print error message
		exit(1);
	}

	printf("\nACTIVATED SERVICES \n");
	while (1)
	{
		fdset = allfdset;   //Sets the fdset that will be checked by the select to the complete set of file descriptors of the activable services
		maxfd = getMaxFd(); //Calculate the maxfd the select method will use
		temp = select(maxfd + 1, &fdset, NULL, NULL, &timeout);

		//Error handling based on the select return value
		if (temp == 0)
		{ //Timout expired
			printf("Timeout expired, no pending connection on socket\n");
			break;
		}
		else if (temp == -1)
		{ //Select call got interrupted
			if (errno == EINTR)
			{ //If the select got interrupted because of a signal
				fdset = allfdset;
				continue; //Go back to the select sys-call
			}
			else
			{
				perror("select error"); //Print error message
				exit(1);
			}
		}
		count++; //Counts in which iteration of the while we are in

		//Find the services ready to be read
		for (i = 0; i < size; i++)
		{
			if (FD_ISSET(services[i].fd, &fdset))
			{ //If the file descriptor to the service is in fdset, the service is ready
				Service actual_service = services[i];

				// If it is a TCP service, create the connection socket
				if (strcmp(actual_service.transport_protocol, TCP) == 0)
				{
					cli_size = sizeof(client_addr);
					clientfd = accept(actual_service.fd, (struct sockaddr *)&client_addr, &cli_size);
					if (clientfd < 0)
					{
						perror("accept error"); // Print error message
						exit(EXIT_FAILURE);
					}
				}

				//Create a child process which is going to execute the required service
				pid = fork();
				if (pid < 0)
				{
					perror("fork error"); // Print error message
					exit(EXIT_FAILURE);
				}
				else if (pid == 0)
				{
					//Son instructions
					//Close all standard I/O
					close(0);
					close(1);
					close(2);

					//Stdin, stdout and sterr re-assignment
					if (strcmp(actual_service.transport_protocol, TCP) == 0)
					{							  //If it is a TCP server
						close(actual_service.fd); //Close the welcome socket
						dup(clientfd);			  //Re-assign 0, 1 and 2 to the connection socket
						dup(clientfd);
						dup(clientfd);
					}
					else if (strcmp(actual_service.transport_protocol, UDP) == 0)
					{							//If it is an UDP server
						dup(actual_service.fd); //Re-assign 0, 1 and 2 to the UDP server socket
						dup(actual_service.fd);
						dup(actual_service.fd);
					}

					//Execute the required service
					execle(actual_service.path, actual_service.name, (char *)NULL, env);

					if (strcmp(actual_service.transport_protocol, TCP) == 0)
					{					 //If it is a TCP server
						close(clientfd); //Close the connection socket (the welcome one is already closed)
					}
					else if (strcmp(actual_service.transport_protocol, UDP) == 0)
					{							  //If it is an UDP server
						close(actual_service.fd); //Closes the UDP server socket
					}

					return 0;
				}
				else
				{
					//Father instructions
					//Print the information about the service which has just been activated
					printf("ciclo while: %d; app: %s %s %s %s fd:%d, pid:%d \n", count, services[i].path, services[i].transport_protocol, services[i].port, services[i].service_mode, services[i].fd, pid);

					if (strcmp(actual_service.transport_protocol, TCP) == 0)
					{					 //If it is a TCP service
						close(clientfd); //Closes connections socket
					}

					if (strcmp(actual_service.service_mode, "wait") == 0)
					{										  //If it is a wait service
						services[i].pid = pid;				  //Saves the pid of the process which is currently using this service
						FD_CLR(actual_service.fd, &allfdset); //Removes the fd of the services from the avaiable ones
					}

					//In case of an UDP nowait service, waits a little to give the opportunity to the first UDPserver created to actually read the ready fd,
					// avoiding it to still be ready on the next select iteration
					if ((strcmp(actual_service.transport_protocol, "udp") == 0 && strcmp(actual_service.service_mode, "nowait") == 0))
					{
						sleep(5);
					}
				}
			}
		}
	}
	return 0;
}

//SIGCHLD handling
void handle_signal(int sig)
{
	int pid;
	int i = 0;
	Service actual_service;
	// Call to wait system-call to get the pid of the dead child process
	pid = wait(NULL);

	//Find the service associated with the pid
	for (i = 0; i < size; i++)
	{
		if (strcmp(services[i].service_mode, "wait") == 0 && services[i].pid == pid)
		{
			actual_service = services[i];

			//Eventually re-insert the fd in the fdset
			switch (sig)
			{
			case SIGCHLD:							  //If the sig is a SIGCHLD
				FD_SET(actual_service.fd, &allfdset); //Re-insert the service's fd in the fdset
				printf("PID SIGCHLD:%d \n", pid);
				break;
			default:
				printf("Signal not known!\n");
				break;
			}
			break;
		}
	}
}

//Get the highest fd number in the set of the fds for the currently activable services
int getMaxFd()
{
	int maxfd = 0;
	for (int i = 0; i < size; i++)
	{
		if (services[i].fd > maxfd && FD_ISSET(services[i].fd, &allfdset))
		{
			maxfd = services[i].fd;
		}
	}
	return maxfd;
}

//Extract informations about the services by reading the config file
int extract_services(Service *services, int max_size)
{
	FILE *fd;
	int res;
	int i = 0;
	char string[100];
	fd = fopen("config", "r");
	if (fd == NULL)
	{
		perror("Error in file opening");
		exit(1);
	}
	res = fscanf(fd, "%s", string);
	do
	{
		strcpy(services[i].path, string);
		getName(string, services[i].name);
		res = fscanf(fd, "%s", string);
		strcpy(services[i].transport_protocol, string);
		res = fscanf(fd, "%s", string);
		strcpy(services[i].port, string);
		res = fscanf(fd, "%s", string);
		strcpy(services[i].service_mode, string);

		res = fscanf(fd, "%s", string);
		i++;
	} while (res != EOF && i < max_size);
	return i;
}

//Gets the service's name using its path
void getName(char *path, char *name)
{
	char *last = strrchr(path, '/');
	if (last != NULL)
	{
		last++;
		strcpy(name, last);
	}
}
