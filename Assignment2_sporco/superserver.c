#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/time.h>
#include<netinet/in.h>
#include<signal.h>
#include<errno.h>

//Service structure definition goes here
typedef struct {
  char transport_protocol[20]; 
  char service_mode[20]; 
  char port[6];
  char path[200];
  char name[50];
  int fd;
  int pid;
} Service;

#define SERVICES_SIZE 10
#define SERVER_PORT 9000 // Initial server port
#define BACK_LOG 2 // Maximum queued requests

int extract_services(Service* services, int max_size);
void getName(char* path, char *name);


int  main(int argc,char **argv,char **env){ 
	//test lettura da file 
	printf("********** test apertura file ************\n\n");
	Service services[SERVICES_SIZE];
	extract_services(services,SERVICES_SIZE);
	for (int i=0; i<SERVICES_SIZE; i++){
		printf("transport_protocol : %s \n",services[i].transport_protocol);
		printf("service_mode : %s \n",services[i].service_mode);
		printf("port : %s \n",services[i].port);
		printf("path : %s \n",services[i].path);
		printf("name : %s \n",services[i].name);
		printf("-------------------------------------------------------\n\n");
	}
	
	return 0;
}


// extract informations by reading the config files
int extract_services(Service* services, int max_size){
	FILE *fd;
	int res;
	int i = 0;
	char string[100];
	fd=fopen("config", "r"); 
	if( fd==NULL ) {
		perror("Errore in apertura del file");
		exit(1);
	}
	res=fscanf(fd, "%s", string);
	do{
		strcpy(services[i].path, string);
		getName(string, services[i].name);
		res=fscanf(fd, "%s", string);
		strcpy(services[i].transport_protocol, string);
		res=fscanf(fd, "%s", string);
		strcpy(services[i].port, string);
		res=fscanf(fd, "%s", string);
		strcpy(services[i].service_mode, string);

		res=fscanf(fd, "%s", string);
		i++;
	}while(res!=EOF && i<max_size);
	return i;
}

void getName(char* path, char *name){
	char *lastOcc = strrchr(path, '/');
    if (lastOcc != NULL)
    {
    	lastOcc++;
    	strcpy(name, lastOcc);
    }
}





